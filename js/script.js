const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];
const rootBlock = document.createElement('div');

rootBlock.id = 'root';
document.body.prepend(rootBlock);

let ulElement = document.createElement('ul');
rootBlock.appendChild(ulElement);

function validateValues(element) {

    const expectedValues = Object.keys(element);


    if (!(expectedValues.includes('author'))) {
        return `Book: ${element.name}. Expected value: author`
    } else if (!(expectedValues.includes('name'))) {
        return `Book: ${element.name}. Expected value: name`
    } else if (!(expectedValues.includes('price'))) {
        return `Book: ${element.name}. Expected value: price`
    }
}

function createList(elements) {

    const {author, name, price} = elements;
    let liElement = document.createElement('li');

    if (author && name && price) {
        ulElement.appendChild(liElement);
        liElement.innerText = `Автор - ${author}, книга: "${name}", цена: ${price} $`;
    } else {
        throw new Error(`Expected values: ${validateValues(elements)}`)
    }
}

books.forEach((element) => {

    try {
        createList(element)
    } catch (e) {
        console.log(e.message)
    }
})


